module gin

go 1.12

require (
	github.com/EDDYCJY/go-gin-example v0.0.0-20190722044107-2647e8bfcbb9
	github.com/Unknwon/com v0.0.0-20190321035513-0fed4efef755 // indirect
	github.com/astaxie/beego v1.12.0 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-contrib/sse v0.1.0 // indirect
	github.com/gin-gonic/gin v1.4.0
	github.com/go-ini/ini v1.44.0 // indirect
	github.com/go-sql-driver/mysql v1.4.1
	github.com/golang/protobuf v1.3.2 // indirect
	github.com/jinzhu/gorm v1.9.10
	github.com/joho/godotenv v1.3.0
	github.com/json-iterator/go v1.1.7 // indirect
	github.com/liamylian/jsontime v1.0.1 // indirect
	github.com/mattn/go-isatty v0.0.8 // indirect
	github.com/rifflock/lfshook v0.0.0-20180920164130-b9218ef580f5
	github.com/sirupsen/logrus v1.2.0
	github.com/stretchr/objx v0.2.0 // indirect
	github.com/ugorji/go v1.1.7 // indirect
	github.com/xiuno/gin v0.0.0-20190107085609-99a85c75f82b
	golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4
	golang.org/x/net v0.0.0-20190724013045-ca1201d0de80 // indirect
	golang.org/x/sys v0.0.0-20190726091711-fc99dfbffb4e // indirect
	golang.org/x/text v0.3.2 // indirect
	golang.org/x/tools v0.0.0-20190728063539-fc6e2057e7f6 // indirect
)

package mytools

import (
	"gin/Server/logging"

	"github.com/joho/godotenv"
)

func EnvInit() {

	err := godotenv.Load()
	if err != nil {
		logging.Error("Error loading .env file")
	}

	// s3Bucket := os.Getenv("S3_BUCKET")
	// fmt.Println(s3Bucket)

	// secretKey := os.Getenv("SECRET_KEY")
	// fmt.Println(secretKey)

}

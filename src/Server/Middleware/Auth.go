package auth

import (
	"encoding/json"
	models "gin/Server/Models"
	"gin/Server/logging"

	"net/http"

	"github.com/gin-gonic/gin"

	"github.com/EDDYCJY/go-gin-example/pkg/app"
	"github.com/EDDYCJY/go-gin-example/pkg/e"
	"github.com/EDDYCJY/go-gin-example/pkg/util"
)

type tokenStruct struct {
	Token   string
	Expires string
}

const C_AuthUser = "userAuth"
const C_AuthAdmin = "adminAuth"

func GetUserToken(auths ...string) gin.HandlerFunc {
	return func(c *gin.Context) {
		var errSt string
		appG := app.Gin{C: c}
		username := c.PostForm("username")
		password := c.PostForm("password")
		logging.LogPostForm(c)

		isAuthOk, err := models.UserCheck(username, password)
		if err != nil {
			logging.Warn(err)
			appG.Response(http.StatusInternalServerError, e.ERROR_AUTH_TOKEN, nil)
			return
		}

		if isAuthOk == false {
			errSt = "|create token error, check acc & pw"
			logging.Warn(c.ClientIP() + errSt)
			c.JSON(http.StatusUnauthorized, gin.H{"error": errSt})
			c.Abort()
			return
		}

		// 取得userAuth
		tokenArr, err := util.GenerateToken(username, password, C_AuthUser)
		if err != nil {
			logging.Warn(err)
			appG.Response(http.StatusInternalServerError, e.ERROR_AUTH_TOKEN, nil)
			// errSt = "|GenerateToken error"
			// logging.Warn(c.ClientIP() + errSt)
			// c.JSON(http.StatusUnauthorized, gin.H{"error": errSt})
			// c.Abort()
			//appG.Response(http.StatusInternalServerError, e.ERROR_AUTH_TOKEN, nil)
			return
		}

		srcJSON := []byte(tokenArr)
		var result tokenStruct
		err_json := json.Unmarshal(srcJSON, &result)
		if err_json != nil {
			panic(err_json)
		}

		appG.Response(http.StatusOK, e.SUCCESS, map[string]string{
			"token":   result.Token,
			"expires": result.Expires,
		})

		c.Next()
	}
}

func GetAdminToken(auths ...string) gin.HandlerFunc {
	return func(c *gin.Context) {

		appG := app.Gin{C: c}
		username := c.PostForm("username")
		password := c.PostForm("password")
		logging.LogPostForm(c) // log

		isAuthOk, err := models.AdminCheck(username, password)
		if err != nil {
			logging.Warn(err)
			appG.Response(http.StatusInternalServerError, e.ERROR_AUTH_TOKEN, nil)
			return
		}

		if isAuthOk == false {
			logging.Warn("create token error, check acc & pw")
			c.JSON(http.StatusUnauthorized, gin.H{"error": "create token error, check acc & pw"})
			c.Abort()
			return
		}

		// 取得userAuth
		tokenArr, err := util.GenerateToken(username, password, C_AuthAdmin)
		if err != nil {
			logging.Warn(err)
			appG.Response(http.StatusInternalServerError, e.ERROR_AUTH_TOKEN, nil)
			return
		}

		srcJSON := []byte(tokenArr)
		var result tokenStruct
		err_json := json.Unmarshal(srcJSON, &result)
		if err_json != nil {
			panic(err_json)
		}

		appG.Response(http.StatusOK, e.SUCCESS, map[string]string{
			"token":   result.Token,
			"expires": result.Expires,
		})

		c.Next()
	}
}

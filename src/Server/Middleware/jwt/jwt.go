package jwt

import (
	"gin/Server/logging"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"

	"github.com/EDDYCJY/go-gin-example/pkg/e"
	"github.com/EDDYCJY/go-gin-example/pkg/util"
)

// JWT is jwt middleware
func JWT(auth_type string) gin.HandlerFunc {
	return func(c *gin.Context) {
		var code int
		var data interface{}

		code = e.SUCCESS
		token := c.PostForm("token")
		if token == "" {
			code = e.INVALID_PARAMS
		} else {
			tokenResult, err := util.ParseToken(token)
			if err != nil {
				switch err.(*jwt.ValidationError).Errors {
				case jwt.ValidationErrorExpired:
					code = e.ERROR_AUTH_CHECK_TOKEN_TIMEOUT
				default:
					code = e.ERROR_AUTH_CHECK_TOKEN_FAIL
				}
			}

			if code != e.SUCCESS {
				logging.Warn(e.GetMsg(code))
				c.JSON(http.StatusUnauthorized, gin.H{
					"code": code,
					"msg":  e.GetMsg(code),
					"data": data,
				})
				c.Abort()
				return
			}

			// 驗證token type
			if tokenResult.Type != auth_type {
				logging.Warn("token type error")
				c.JSON(http.StatusUnauthorized, gin.H{
					"msg": "token type error",
				})
				c.Abort()
				return
			}
		}

		if code != e.SUCCESS {
			logging.Warn(e.GetMsg(code))
			c.JSON(http.StatusUnauthorized, gin.H{
				"code": code,
				"msg":  e.GetMsg(code),
				"data": data,
			})

			c.Abort()
			return
		}

		c.Next()
	}
}

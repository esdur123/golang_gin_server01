package models

import (
	"errors"
	orm "gin/Server/Database"
	"time"

	"github.com/jinzhu/gorm"
)

type Admin struct {
	Admin_id  uint       `gorm:"primary_key"`
	Name      string     `sql:"type:varchar(50);not null;;comment:'帳號'"`
	Password  string     `sql:"type:varchar(200);not null;;comment:'密碼'"`
	CreatedAt time.Time  `sql:"comment:'創建時間'"`
	UpdatedAt time.Time  `sql:"comment:'更新時間'"`
	DeletedAt *time.Time `sql:"comment:'刪除時間'"`
}

// 創建 DB Table (遷移 Migrate)
func AdminMigrate() {
	orm.Eloquent.AutoMigrate(&Admin{})
}

// 填入初始資料
func AdminSeed() {
	// Seeding tables:
	hash, _ := PasswordHash("123456")
	var seed_admins []Admin = []Admin{
		Admin{Name: "admin", Password: hash},
		Admin{Name: "admin2", Password: hash},
	}

	for _, admin := range seed_admins {
		orm.Eloquent.Create(&admin)
	}
}

var Admins []Admin

func AdminCheck(username, password string) (bool, error) {
	if username == "" || password == "" {
		return false, errors.New("username or password empty")
		panic("username or password empty")
	}

	var admin Admin
	err := orm.Eloquent.Select("admin_id,password").Where(Admin{Name: username}).First(&admin).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return false, err
	}

	if admin.Admin_id > 0 {
		isPwCorrect := PasswordVerify(password, admin.Password)
		return isPwCorrect, nil
	}

	return false, nil
}

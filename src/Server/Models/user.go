package models

import (
	"errors"
	orm "gin/Server/Database"
	"time"

	"github.com/jinzhu/gorm"
)

type User struct {
	User_id   uint       `gorm:"primary_key"`
	Name      string     `sql:"type:varchar(50);not null;;comment:'帳號'"`
	Password  string     `sql:"type:varchar(200);not null;;comment:'密碼'"`
	CreatedAt time.Time  `sql:"comment:'創建時間'"`
	UpdatedAt time.Time  `sql:"comment:'更新時間'"`
	DeletedAt *time.Time `sql:"comment:'刪除時間'"`
}

// 創建 DB Table (遷移 Migrate)
func UserMigrate() {
	orm.Eloquent.AutoMigrate(&User{})
}

// 填入初始資料
func UserSeed() {
	// Seeding tables:
	hash, _ := PasswordHash("123456")
	var seed_users []User = []User{
		User{Name: "nori", Password: hash},
		User{Name: "nori2", Password: hash},
	}

	for _, user := range seed_users {
		orm.Eloquent.Create(&user)
	}
}

// 刪除Table
func UserDrop() {
	orm.Eloquent.DropTableIfExists(&User{}, "users")
}

var Users []User

func UserCheck(username, password string) (bool, error) {

	if username == "" || password == "" {
		return false, errors.New("username or password empty")
		panic("username or password empty")
	}

	var user User
	err := orm.Eloquent.Select("user_id,password").Where(User{Name: username}).First(&user).Error
	if err != nil && err != gorm.ErrRecordNotFound {
		return false, err
	}

	if user.User_id > 0 { // 比對hash
		isPwCorrect := PasswordVerify(password, user.Password)
		return isPwCorrect, nil
	}

	return false, nil
}

// //添加
// func (user User) Insert() (id int64, err error) {

// 	//添加数据
// 	result := orm.Eloquent.Create(&user)
// 	id = user.ID
// 	if result.Error != nil {
// 		err = result.Error
// 		return
// 	}
// 	return
// }

// //列表
// func (user *User) Users() (users []User, err error) {
// 	if err = orm.Eloquent.Find(&users).Error; err != nil {
// 		return
// 	}
// 	return
// }

// //修改
// func (user *User) Update(id int64) (updateUser User, err error) {

// 	if err = orm.Eloquent.Select([]string{"id", "username"}).First(&updateUser, id).Error; err != nil {
// 		return
// 	}

// 	//参数1:是要修改的数据
// 	//参数2:是修改的数据
// 	if err = orm.Eloquent.Model(&updateUser).Updates(&user).Error; err != nil {
// 		return
// 	}
// 	return
// }

// //删除数据
// func (user *User) Destroy(id int64) (Result User, err error) {

// 	if err = orm.Eloquent.Select([]string{"id"}).First(&user, id).Error; err != nil {
// 		return
// 	}

// 	if err = orm.Eloquent.Delete(&user).Error; err != nil {
// 		return
// 	}
// 	Result = *user
// 	return
// }

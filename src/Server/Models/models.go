/*
	呼叫所有Models的functions
*/

package models

import (
	bcrypt "golang.org/x/crypto/bcrypt"
)

// Migrate所有Table
func MigrateAll() {
	UserMigrate()
	AdminMigrate()
}

// Seed所有Table
func SeedAll() {
	UserSeed()
	AdminSeed()
}

// 密碼hash
func PasswordHash(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	return string(bytes), err
}

// 密碼比對
func PasswordVerify(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

/*
table範例
type Product struct {
	ID           uint   		 `gorm:"primary_key"`
	Dfval        int    		 `gorm:"default:'0'"`
	Nool         string 		 `gorm:"type:varchar(100);not null;"`
	Saaa         string 		 `sql:"type:varchar(10);not null;default:'';comment:'Sa註釋'"`
	Jdddd        string 		 `sql:"comment:'Jd註釋'"`
	Email        string          `gorm:"type:varchar(100);unique_index"`
	Role         string          `gorm:"size:255"`        // set field size to 255
	MemberNumber *string         `gorm:"unique;not null"` // set member number to unique and not null
	Num          int             `gorm:"AUTO_INCREMENT"`  // set num to auto incrementable
	Address      string          `gorm:"index:addr"`      // create index with name `addr` for address
	IgnoreMe     int             `gorm:"-"`               // ignore this field
	Thumbnails   json.RawMessage `gorm:"default:'[]'"`
	Ttaa         string          `sql:"type:text"`
	Birthday     *time.Time  // null
	CreatedAt    time.Time   // 自動填入目前時間
	UpdatedAt    time.Time	 // 自動填入目前時間
	DeletedAt    *time.Time  // null
}
*/

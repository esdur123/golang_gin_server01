package route

import (
	exampleController "gin/Server/Controllers/Api/ExampleController"
	viewController "gin/Server/Controllers/View/ViewController"
	auth "gin/Server/Middleware"

	"gin/Server/Middleware/jwt"
	"os"

	"github.com/gin-gonic/gin"
)

func Init(port string) {
	ginMode := os.Getenv("GIN_MODE")
	if ginMode == "release" {
		gin.SetMode(gin.ReleaseMode)
	} else {
		gin.SetMode(gin.DebugMode)
	}

	router := gin.New()
	// Logger middleware will write the logs to gin.DefaultWriter even if you set with GIN_MODE=release.
	// By default gin.DefaultWriter = os.Stdout
	//router.Use(gin.Logger())
	// Recovery middleware recovers from any panics and writes a 500 if there was one.
	router.Use(gin.Recovery())

	//router.Use(mylog.SaveLog())

	router.POST("/auth/user", auth.GetUserToken())

	user := router.Group("/api/user", jwt.JWT(auth.C_AuthUser))
	{
		user.POST("/post", exampleController.Post)
		//user.GET("/get/query", exampleController.GetQuery)
		// user.GET("/get/param/:name/:pw", exampleController.GetParam)

		// user.POST("/post", exampleController.Post)
		// user.POST("/post/body/raw", exampleController.PostBodyRaw)
		// user.POST("/post/form", exampleController.PostForm)
		// user.POST("/post/query/:id/:page", exampleController.PostQuery)
	}

	router.POST("/auth/admin", auth.GetAdminToken())
	admin := router.Group("/api/admin", jwt.JWT(auth.C_AuthAdmin))
	{
		admin.POST("/post2", exampleController.Post2)
		//admin.GET("/get/query", exampleController.GetQuery)
		// admin.GET("/get/param/:name/:pw", exampleController.GetParam)

		// admin.POST("/post", exampleController.Post)
		// admin.POST("/post/body/raw", exampleController.PostBodyRaw)
		// admin.POST("/post/form", exampleController.PostForm)
		// admin.POST("/post/query/:id/:page", exampleController.PostQuery)
	}

	// Html
	router.LoadHTMLGlob("View/*")
	html := router.Group("/html")
	{
		html.GET("/", viewController.Index)

	}

	// router.GET("/get", exampleController.Get)
	// router.GET("/get/query", exampleController.GetQuery)
	// router.GET("/get/param/:name/:pw", exampleController.GetParam)

	// router.POST("/post", exampleController.Post)
	// router.POST("/post/body/raw", exampleController.PostBodyRaw)
	// router.POST("/post/form", exampleController.PostForm)
	// router.POST("/post/query/:id/:page", exampleController.PostQuery)
	runPout := ":"
	runPout += port
	router.Run(runPout) // listen and serve on 0.0.0.0:8080

}

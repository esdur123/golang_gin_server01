package logging

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"runtime"

	"github.com/EDDYCJY/go-gin-example/pkg/file"
	"github.com/gin-gonic/gin"
)

type Level int

type PostStruct struct { // 紀錄PostForm
	PostKey string
	PostVal []string
}

var (
	F *os.File

	DefaultPrefix      = ""
	DefaultCallerDepth = 2

	logger     *log.Logger
	logPrefix  = ""
	levelFlags = []string{"DEBUG", "INFO", "WARN", "ERROR", "FATAL", "POST", "GET"}
)

const (
	DEBUG Level = iota
	INFO
	WARNING
	ERROR
	FATAL
	POST
	GET
)

// Setup initialize the log instance
func Init() {
	var err error
	filePath := getLogFilePath()
	fileName := getLogFileName()
	F, err = file.MustOpen(fileName, filePath)
	if err != nil {
		log.Fatalf("logging.Setup err: %v", err)
	}

	logger = log.New(F, DefaultPrefix, log.LstdFlags)
}

// 針對PostForm紀錄的log
func LogPostForm(c *gin.Context) {
	apiPath := []string{c.Request.URL.String()}
	posts := []PostStruct{PostStruct{PostKey: c.ClientIP(), PostVal: apiPath}}
	for key, value := range c.Request.PostForm {
		posts = append(posts, PostStruct{PostKey: key, PostVal: value})
	}
	Post(posts)
}

// Debug output logs at debug level
func Debug(v ...interface{}) {
	setPrefix(DEBUG)
	logger.Println(v)
}

// Info output logs at info level
func Info(v ...interface{}) {
	setPrefix(INFO)
	logger.Println(v)
}

// post
func Post(v ...interface{}) {
	//setPrefix(POST)
	//fmt.Sprintf("[POST]")
	logger.SetPrefix("[POST]")
	logger.Println(v)
}

// get
func Get(v ...interface{}) {
	setPrefix(GET)
	logger.Println(v)
}

// Warn output logs at warn level
func Warn(v ...interface{}) {
	setPrefix(WARNING)
	logger.Println(v)
}

// Error output logs at error level
func Error(v ...interface{}) {
	setPrefix(ERROR)
	logger.Println(v)
}

// Fatal output logs at fatal level
func Fatal(v ...interface{}) {
	setPrefix(FATAL)
	logger.Fatalln(v)
}

// setPrefix set the prefix of the log output
func setPrefix(level Level) {
	_, file, line, ok := runtime.Caller(DefaultCallerDepth)
	if ok {
		logPrefix = fmt.Sprintf("[%s][%s:%d]", levelFlags[level], filepath.Base(file), line)
	} else {
		logPrefix = fmt.Sprintf("[%s]", levelFlags[level])
	}

	logger.SetPrefix(logPrefix)
}

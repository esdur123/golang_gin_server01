package viewController

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// path="/"
func Index(c *gin.Context) {
	c.HTML(http.StatusOK, "index.html", gin.H{
		"title": "hello Go 我是 html title",
	})
}

package exampleController

import (
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/gin-gonic/gin"
)

func Get(c *gin.Context) {
	// c.JSON(200, gin.H{
	// 	"name": name,
	// 	"pw":   pw,
	// })
	c.JSON(http.StatusOK, map[string]string{
		"user": "get_1a",
		"val":  "get_1b",
	})
}

func Get2(c *gin.Context) {
	// c.JSON(200, gin.H{
	// 	"name": name,
	// 	"pw":   pw,
	// })
	c.JSON(http.StatusOK, map[string]string{
		"admin": "get_2a",
		"val":   "get_2b",
	})
}

func GetQuery(c *gin.Context) {
	name := c.Query("name")
	pw := c.Query("pw")
	// c.JSON(200, gin.H{
	// 	"name": name,
	// 	"pw":   pw,
	// })
	c.JSON(http.StatusOK, map[string]string{
		"name": name,
		"pw":   pw,
	})
}

func GetParam(c *gin.Context) {
	name := c.Param("name")
	pw := c.Param("pw")
	// c.JSON(200, gin.H{
	// 	"name": name,
	// 	"pw":   pw,
	// })
	c.JSON(http.StatusOK, map[string]string{
		"name": name,
		"pw":   pw,
	})
}

func Post(c *gin.Context) {
	// name := c.Param("name")
	// pw := c.Param("pw")
	c.JSON(200, gin.H{
		"恭喜您":  "成功驗證JWT使用USER API",
		"Post": "測試成功",
	})
	// c.JSON(http.StatusOK, map[string]string{
	// 	"name": name,
	// 	"pw":   pw,
	// })
}

func Post2(c *gin.Context) {
	// name := c.Param("name")
	// pw := c.Param("pw")
	c.JSON(200, gin.H{
		"恭喜您":  "成功驗證JWT使用ADMIN API",
		"Post": "ADMIN Post成功",
	})
	// c.JSON(http.StatusOK, map[string]string{
	// 	"name": name,
	// 	"pw":   pw,
	// })
}

func PostBodyRaw(c *gin.Context) {
	body := c.Request.Body
	value, err := ioutil.ReadAll(body)
	if err != nil {
		fmt.Println(err.Error())
	}

	c.JSON(200, gin.H{
		"message": string(value),
	})
}

func PostForm(c *gin.Context) {

	acc := c.PostForm("acc")
	pw := c.PostForm("pw")

	result := map[string]string{
		"acc": acc,
		"pw":  pw,
	}

	c.JSON(200, gin.H{
		"message": result,
	})
}

func PostQuery(c *gin.Context) {
	id := c.Param("id")     //查询请求URL后面的参数
	page := c.Param("page") //查询请求URL后面的参数，如果没有填写默认值

	result := map[string]string{
		"id":   id,
		"page": page,
	}

	c.JSON(200, gin.H{
		"message": result,
	})
}

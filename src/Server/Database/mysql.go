package database

import (
	"fmt"
	"os"

	"gin/Server/logging"

	_ "github.com/go-sql-driver/mysql" //加载mysql
	"github.com/jinzhu/gorm"
)

var Eloquent *gorm.DB

func Init() {
	var err error

	Eloquent, err = gorm.Open(os.Getenv("DB_CONNECTION"), fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local",
		os.Getenv("DB_USERNAME"),
		os.Getenv("DB_PASSWORD"),
		os.Getenv("DB_HOST"),
		os.Getenv("DB_PORT"),
		os.Getenv("DB_DATABASE")))

	if err != nil {
		logging.Error(err)
		//fmt.Printf("mysql connect error %v", err)
	}

	if Eloquent.Error != nil {
		logging.Error(err)
		//fmt.Printf("database error %v", Eloquent.Error)
	}

	//Eloquent.LogMode(true)

}

func Exit() {
	defer Eloquent.Close()
}

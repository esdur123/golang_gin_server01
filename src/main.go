package main

import (
	orm "gin/Server/Database"
	models "gin/Server/Models"
	route "gin/Server/Route"
	mytools "gin/Server/Tools"
	logging "gin/Server/logging"
)

func main() {
	isMigrateSeed := false // (first time only)
	logging.Init()
	mytools.EnvInit() // env
	orm.Init()        // db connect
	if isMigrateSeed {
		models.MigrateAll() // migrate
		models.SeedAll()    // seed
	}

	route.Init("8080") // router

}

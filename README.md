-編修中

-簡介
 
    -使用 router(gin), env(godotenv), db(gorm), runtime log
    
    -jwt調整為回傳token和時間戳記, 並分user和admin兩種token
    
    -log 會依日期於Server/Runtime/logs資料夾內生+寫入
     可在Server/logging/file.go調整path
     
    -密碼PasswordHash()比對驗證 

-環境設定
    
    -請先安裝go+設定環境變數 (參考網頁)

        https://oranwind.org/go-go-yu-yan-yu-windows-shang-zhi-an-zhuang-yu-huan-jing-she-ding/
    
    -請於.env檔填寫你的DB
     並開啟您的Xampp Database或其他DB
    
    -將mail.go裡有first-time only的那三行 槓號取消
     (等等要做一次db migrate和seed)
    
    -開一個Terminal (VSCode上方Treminal->New Terminal)
     在Terminal視窗最右邊選Terminal
     執行指令 go run main.go
     此時系統會自動添加 import 需要的github檔案
     同時 此次會執行migrate+seed
     
    -系統一定會報錯util檔案錯誤相關
     因為我有改github裡面的程式
     所以請根據自己的user位置修正覆蓋此檔案
     C:\Users\User\go\pkg\mod\github.com\!e!d!d!y!c!j!y\go-gin-example@v0.0.0-20190722044107-2647e8bfcbb9\pkg\util\jwt.go
     
>         package util
>         
>         import (
>         	"encoding/json"
>         	"strconv"
>         	"time"
>         
>         	"github.com/dgrijalva/jwt-go"
>         )
>         
>         var jwtSecret []byte
>         
>         type Claims struct {
>         	Username string `json:"username"`
>         	Password string `json:"password"`
>         	Type     string `json:"type"`
>         	jwt.StandardClaims
>         }
>         
>         type tokenStruct struct {
>         	Token   string `json:"token"`
>         	Expires string `json:"expires"`
>         }
>         
>         // GenerateToken generate tokens used for auth
>         func GenerateToken(username, password, tokentype string) (string, error) {
>         	nowTime := time.Now()
>         	expireTime := nowTime.Add(3 * time.Hour)
>         	timestamp := expireTime.Unix()
>         	stimestamp := strconv.FormatInt(timestamp, 10)
>         
>         	claims := Claims{
>         		EncodeMD5(username),
>         		EncodeMD5(password),
>         		tokentype,
>         		jwt.StandardClaims{
>         			ExpiresAt: expireTime.Unix(),
>         			Issuer:    "Issuer_01",
>         		},
>         	}
>         
>         	tokenClaims := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
>         	token, err := tokenClaims.SignedString(jwtSecret)
>         
>         	// 也回傳expires 包成json
>         	returnToken := tokenStruct{Token: token, Expires: stimestamp}
>         	b, _ := json.Marshal(returnToken)
>         
>         	return string(b), err
>         }
>         
>         // ParseToken parsing token
>         func ParseToken(token string) (*Claims, error) {
>         	tokenClaims, err := jwt.ParseWithClaims(token, &Claims{}, func(token *jwt.Token) (interface{}, error) {
>         		return jwtSecret, nil
>         	})
>         
>         	//fmt.Printf("%d", tokenClaims)
>         
>         	if tokenClaims != nil {
>         		if claims, ok := tokenClaims.Claims.(*Claims); ok && tokenClaims.Valid {
>         			return claims, nil
>         		}
>         	}
>         
>         	return nil, err
>         }

    
    -修改之後, 請將main.go原本first-time only那三行重新槓掉
     (此時db應該已新增user和admin table和資料)
     
    -再次執行go run main.go (或直接按F5)
     如果可以在Terminal視窗 右邊第二個 Debug Console內看到
     
     
     
>         [GIN-debug] [WARNING] Running in "debug" mode. Switch to "release" mode in production.
>          - using env:	export GIN_MODE=release
>          - using code:	gin.SetMode(gin.ReleaseMode)
>         
>         [GIN-debug] POST   /auth/user                --> gin/Server/Middleware.GetUserToken.func1 (2 handlers)
>         [GIN-debug] POST   /api/user/post            --> gin/Server/Controllers/Api/ExampleController.Post (3 handlers)
>         [GIN-debug] POST   /auth/admin               --> gin/Server/Middleware.GetAdminToken.func1 (2 handlers)
>         [GIN-debug] POST   /api/admin/post2          --> gin/Server/Controllers/Api/ExampleController.Post2 (3 handlers)
>         [GIN-debug] Loaded HTML Templates (2): 
>         	- 
>         	- index.html
>         
>         [GIN-debug] GET    /html/                    --> gin/Server/Controllers/View/ViewController.Index (2 handlers)
>         [GIN-debug] Listening and serving HTTP on :8080
>         Failed to get threads - Process 9500 has exited with status 0
>         Process exiting with code: 0    
 
     
    代表環境設定成功
    
    -前端網頁
     http://localhost:8080/html/
     可看 "hello Go 我是 html title"
     
    -登入取得 User Token
     打開Postman
     POST 
     http://localhost:8080/auth/user
     參數 username  nori
          password  123456
     API會回傳格式 token和時間戳記
        {
            "code": 200,
            "msg": "ok",
            "data": {
                "expires": "",
                "token": ""
            }
        }
     
    -用User的token驗證jwt進入api
     打開Postman
     POST 
     http://localhost:8080/api/user/post
     參數 token 填入剛才得到的token
     API會回傳格式
        {
            "Post": "測試成功",
            "恭喜您": "成功驗證JWT使用USER API"
        } 

    -登入取得 Admin Token
     打開Postman
     POST 
     http://localhost:8080/auth/admin
     參數 username  admin
          password  123456
     API會回傳格式 token和時間戳記
        {
            "code": 200,
            "msg": "ok",
            "data": {
                "expires": "",
                "token": ""
            }
        }
     
    -用Admin的token驗證jwt進入api
     打開Postman
     POST 
     http://localhost:8080/api/admin/post2
     參數 token 填入剛才得到的token
     API會回傳格式
        {
            "Post": "ADMIN Post成功",
            "恭喜您": "成功驗證JWT使用ADMIN API"
        }  
        
-補充

    -log 
     -LogPostForm(c)
      記錄呼叫過POST所有參數
      請手動於c.PostForm("參數")之後
      添加這行logging.LogPostForm(c))
      參考Server/Middleware/Auth.go 第31行
     
     -Warn(string), Info(string)...
      Server/logging/log.go裡面有很多層級的func可呼叫
      舉例 
      logging.Warn(err) 個別紀錄警告
      參考Server/Middleware/Auth.go 第35行
     
    